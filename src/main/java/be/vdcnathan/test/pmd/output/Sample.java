package be.vdcnathan.test.pmd.output;

public class Sample {
    private String content;
    private Label label;

    public Sample(String content, Label label) {
        this.content = content;
        this.label = label;
    }

    public static Sample of(String content, Label label) {
        return new Sample(content, label);
    }

    public String getContent() {
        return content;
    }

    public Label getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "content='" + content + '\'' +
                ", label=" + label +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sample sample = (Sample) o;

        if (content != null ? !content.equals(sample.content) : sample.content != null) return false;
        return label == sample.label;
    }

    @Override
    public int hashCode() {
        int result = content != null ? content.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }
}
