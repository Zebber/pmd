package be.vdcnathan.test.pmd.output;

public class MethodBoundary {

    int begin;
    int end;

    public MethodBoundary(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }
}
