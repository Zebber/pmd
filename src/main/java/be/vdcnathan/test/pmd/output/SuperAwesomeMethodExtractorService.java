package be.vdcnathan.test.pmd.output;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class SuperAwesomeMethodExtractorService implements MethodExtractorService {

    @Override
    public Set<MethodRepresentation> extract(File classFile) throws IOException {
        MethodExtractor methodExtractor = new MethodExtractor();
        return methodExtractor.getMethodRepresentations(classFile);
    }

}
