package be.vdcnathan.test.pmd.output;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class MethodExtractor {

    public Set<MethodRepresentation> getMethodRepresentations(File file) throws IOException {
        CompilationUnit compilationUnit = this.createCompilationUnit(file);
        MethodVisitor visitor = new MethodVisitor();
        visitor.visit(compilationUnit, null);

        Set<MethodBoundary> methodBoundaries = visitor.getMethodBoundaries();
        Set<MethodRepresentation> methodRepresentations = new HashSet<>();

        methodBoundaries.forEach(methodBoundary -> {
            try {
                methodRepresentations.add(readLinesFromFile(file, methodBoundary));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        return methodRepresentations;
    }

    private CompilationUnit createCompilationUnit(File file) throws IOException {
        InputStream in = null;
        CompilationUnit cu;
        try {
            in = new FileInputStream(file);
            cu = JavaParser.parse(in);
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return cu;
    }

    private MethodRepresentation readLinesFromFile(File file, MethodBoundary methodBoundary) throws FileNotFoundException {
        StringBuffer stringBuffer = new StringBuffer();
        int maxWidth = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            int lineNumber = 1;
            boolean lineAdded = false;
            for(String line; (line = br.readLine()) != null; ) {
                if ( methodBoundary.getBegin() <= lineNumber && lineNumber <= methodBoundary.getEnd()) {
                    if (lineAdded) {
                        stringBuffer.append("\n");
                    }
                    stringBuffer.append(line);
                    lineAdded = true;
                    maxWidth = Math.max(maxWidth, line.length());
                }
                ++lineNumber;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new MethodRepresentation(
                stringBuffer.toString(),
                methodBoundary.getBegin(),
                methodBoundary.getEnd(),
                maxWidth,
                methodBoundary.getEnd() - methodBoundary.getBegin() + 1);
    }

}
