package be.vdcnathan.test.pmd.output;

import be.vdcnathan.test.pmd.pmd.PMDFile;
import com.google.common.collect.Sets;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class PMDFileSampler {
    private MethodExtractorService methodExtractorService;

    public PMDFileSampler(MethodExtractorService methodExtractorService) {
        this.methodExtractorService = methodExtractorService;
    }

    public Set<Sample> sample(PMDFile pmdFile) {
        try {
            Set<MethodRepresentation> methodRepresentations = methodExtractorService.extract(pmdFile.asJavaFile());

            return methodRepresentations.stream()
                    .filter(MethodRepresentation::isSmallEnough)
                    .map(methodRepresentation -> toSample(methodRepresentation, pmdFile))
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
            return Sets.newHashSet();
        }
    }

    private Sample toSample(MethodRepresentation methodRepresentation, PMDFile pmdFile) {
        if(containsWarning(methodRepresentation, pmdFile)) {
            return Sample.of(methodRepresentation.getContent(), Label.DIRTY);
        }
        return Sample.of(methodRepresentation.getContent(), Label.CLEAN);
    }

    private boolean containsWarning(MethodRepresentation methodRepresentation, PMDFile pmdFile) {
        return pmdFile.getLines()
                .stream()
                .filter(warningLine -> warningLine <= methodRepresentation.getEndLine())
                .anyMatch(warningLine -> warningLine >= methodRepresentation.getStartLine());
    }
}
