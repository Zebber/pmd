package be.vdcnathan.test.pmd.output;

public class MethodRepresentation {
    private String content;
    private Integer startLine;
    private Integer endLine;
    private Integer width;
    private Integer height;

    public MethodRepresentation(String content, Integer startLine, Integer endLine, Integer width, Integer height) {
        this.content = content;
        this.startLine = startLine;
        this.endLine = endLine;
        this.width = width;
        this.height = height;
    }

    public String getContent() {
        return content;
    }

    public Integer getStartLine() {
        return startLine;
    }

    public Integer getEndLine() {
        return endLine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MethodRepresentation that = (MethodRepresentation) o;

        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (startLine != null ? !startLine.equals(that.startLine) : that.startLine != null) return false;
        if (endLine != null ? !endLine.equals(that.endLine) : that.endLine != null) return false;
        if (width != null ? !width.equals(that.width) : that.width != null) return false;
        return height != null ? height.equals(that.height) : that.height == null;
    }

    @Override
    public int hashCode() {
        int result = content != null ? content.hashCode() : 0;
        result = 31 * result + (startLine != null ? startLine.hashCode() : 0);
        result = 31 * result + (endLine != null ? endLine.hashCode() : 0);
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        return result;
    }

    public boolean isSmallEnough() {
        return width <= 120 && height <= 40;
    }
}
