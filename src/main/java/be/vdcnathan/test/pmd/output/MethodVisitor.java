package be.vdcnathan.test.pmd.output;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.HashSet;
import java.util.Set;

public class MethodVisitor extends VoidVisitorAdapter {

    private final Set<MethodBoundary> methodBoundaries;

    MethodVisitor() {
        methodBoundaries = new HashSet<>();
    }

    public void visit(MethodDeclaration n, Object arg) {
        this.methodBoundaries.add(new MethodBoundary(
                n.getBegin().map(position -> position.line).orElse(-1),
                n.getEnd().map(position -> position.line).orElse(-1)));
    }

    public Set<MethodBoundary> getMethodBoundaries() {
        return this.methodBoundaries;
    }
}