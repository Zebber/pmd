package be.vdcnathan.test.pmd.output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

public interface MethodExtractorService {
    Set<MethodRepresentation> extract(File classFile) throws IOException;
}
