package be.vdcnathan.test.pmd.pmd;

import be.vdcnathan.test.pmd.commandline.CommandLineArgument;
import be.vdcnathan.test.pmd.commandline.CommandLineExecutor;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by nathanv on 26/05/2017.
 */
public class PMDClient {

    private final String rulesets;
    private final CommandLineExecutor commandLineExecutor;

    private final PMDLineParser pmdLineParser = new PMDLineParser();

    protected PMDClient(String rulesets,
                        CommandLineExecutor commandLineExecutor) {
        this.rulesets = rulesets;
        this.commandLineExecutor = commandLineExecutor;
    }

    public List<PMDLine> run(File file) {
        List<String> strings = commandLineExecutor.run(
                        CommandLineArgument.createFor("-dir", file.getAbsolutePath()),
                        CommandLineArgument.createFor("-format", "text"),
                        CommandLineArgument.createFor("-rulesets",
                                                      rulesets)
        );

        return strings.stream()
                .map(pmdLineParser::parseLine)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

}
