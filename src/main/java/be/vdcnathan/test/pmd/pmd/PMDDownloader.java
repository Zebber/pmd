package be.vdcnathan.test.pmd.pmd;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by nathanv on 26/05/2017.
 */
public class PMDDownloader {

    public void downloadAndExtractPMD(URL url,
                                      File outputFolder) {
        File pmdFile = new File("pmd.zip");

        if (outputFolder.exists()) {
            if (new File(outputFolder, "bin").exists()) {
                return;
            }
            deleteDirectory(outputFolder);
        }

        if (!pmdFile.exists()) {
            downloadFile(pmdFile, url);
        }

        extractFile(pmdFile, outputFolder);
    }

    private void deleteDirectory(File outputFolder) {
        try {
            FileUtils.forceDelete(outputFolder);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void extractFile(File pmdFile,File outputFolder) {
        try {
            ZipFile zipFile = new ZipFile(pmdFile);

            File pmdDownloadLocation = new File(FileUtils.getTempDirectory(), "pmd-downloader");


            if (pmdDownloadLocation.exists()) {
                deleteDirectory(pmdDownloadLocation);
            }

            zipFile.extractAll(pmdDownloadLocation.getAbsolutePath());

            File[] files = pmdDownloadLocation.listFiles();

            if (files == null || files.length != 1) {
                throw new RuntimeException("Expected one and only one file");
            }

            FileUtils.moveDirectory(files[0], outputFolder);

        } catch (ZipException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void downloadFile(File pmdFile,
                              URL url) {
        try {
            FileUtils.copyURLToFile(url, pmdFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
