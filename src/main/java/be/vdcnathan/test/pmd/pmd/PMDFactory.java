package be.vdcnathan.test.pmd.pmd;

import be.vdcnathan.test.pmd.commandline.CommandLineExecutor;

import java.io.File;
import java.net.URL;

/**
 * Created by nathanv on 26/05/2017.
 */
public class PMDFactory {

    PMDDownloader pmdDownloader = new PMDDownloader();

    public PMDClient getPMDClient(URL url,
                                  String rulesets) {
        File outputFolder = new File("pmd");

        pmdDownloader.downloadAndExtractPMD(url, outputFolder);

        return new PMDClient(rulesets, new CommandLineExecutor(baseCommand()).runAt(new File(outputFolder, "bin")));
    }

    private String baseCommand() {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            return "cmd /c pmd.bat";
        }
        return "sh run.sh pmd";
    }

}
