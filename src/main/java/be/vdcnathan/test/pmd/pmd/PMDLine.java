package be.vdcnathan.test.pmd.pmd;

/**
 * Created by nathanv on 26/05/2017.
 */
public class PMDLine {

    private final String file;
    private final int line;
    private final String cause;

    public PMDLine(String file,
                   int line,
                   String cause) {
        this.file = file;
        this.line = line;
        this.cause = cause;
    }

    private PMDLine(String file,
                    String cause) {
        this(file, -1, cause);
    }

    public String getFile() {
        return file;
    }

    public int getLine() {
        return line;
    }

    public String getCause() {
        return cause;
    }

    public String asString() {
        return String.format("Found in: %s, at line %s, because of %s", file, line, cause);
    }

    public static PMDLine of(String location,
                             String cause) {
        return new PMDLine(location, cause);
    }

    public static PMDLine of(String file,
                             int line,
                             String cause) {
        return new PMDLine(file, line, cause);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PMDLine pmdLine = (PMDLine) o;

        if (line != pmdLine.line) return false;
        if (file != null ? !file.equals(pmdLine.file) : pmdLine.file != null) return false;
        return cause != null ? cause.equals(pmdLine.cause) : pmdLine.cause == null;
    }

    @Override
    public int hashCode() {
        int result = file != null ? file.hashCode() : 0;
        result = 31 * result + line;
        result = 31 * result + (cause != null ? cause.hashCode() : 0);
        return result;
    }
}
