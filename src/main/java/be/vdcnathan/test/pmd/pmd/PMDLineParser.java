package be.vdcnathan.test.pmd.pmd;

import java.util.Optional;

/**
 * Created by nathanv on 26/05/2017.
 */
public class PMDLineParser {

    public Optional<PMDLine> parseLine(String line) {
        try {
            String[] splitedLine = line.split(":");
            return Optional.of(PMDLine.of(splitedLine[0], Integer.parseInt(splitedLine[1]), splitedLine[2]));
        } catch (Exception e) {
            System.err.println(line);
            return Optional.empty();
        }
    }
}
