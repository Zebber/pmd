package be.vdcnathan.test.pmd.pmd;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PMDFile {
    private String file;
    private List<Integer> lines;

    public PMDFile(String file, List<Integer> lines) {
        this.file = file;
        this.lines = lines;
    }

    public static PMDFile of(Map.Entry<String, List<PMDLine>> pmdFile) {

        List<Integer> lines = pmdFile.getValue()
                .stream()
                .map(PMDLine::getLine)
                .collect(Collectors.toList());

        return new PMDFile(pmdFile.getKey(), lines);
    }

    public File asJavaFile() {
        return new File(file);
    }

    public List<Integer> getLines() {
        return lines;
    }
}
