package be.vdcnathan.test.pmd.commandline;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nathanv on 29/05/2017.
 */
public class CommandLineExecutor {

    private final String baseCommand;
    private File workingDirectory;

    public CommandLineExecutor(String baseCommand) {
        this.baseCommand = baseCommand;
    }

    public List<String> run(CommandLineArgument... arguments) {
        return run(Lists.newArrayList(arguments));
    }

    public List<String> run(List<CommandLineArgument> arguments) {
        try {
            String command = String.format("%s %s",
                                           binaryCommand(),
                                           arguments
                                                   .stream()
                                                   .map(CommandLineArgument::asString)
                                                   .collect(Collectors.joining(" ")));

            Process process = Runtime.getRuntime().exec(command, null, workingDirectory.getAbsoluteFile());

            return readFromProcess(process);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> readFromProcess(Process process) throws IOException {
        List<String> result = Lists.newArrayList();
        InputStreamReader input = new InputStreamReader(process.getInputStream());
        while (process.isAlive()) {
            if (input.ready()) {
                result.addAll(IOUtils.readLines(input));
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        input.close();

        return result;
    }

    private String binaryCommand() {
        return baseCommand;
    }

    public CommandLineExecutor runAt(File workingDirectory) {
        this.workingDirectory = workingDirectory;
        return this;
    }
}
