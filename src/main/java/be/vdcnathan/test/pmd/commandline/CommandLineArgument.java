package be.vdcnathan.test.pmd.commandline;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nathanv on 29/05/2017.
 */
public class CommandLineArgument {
    private final List<String> arguments = Lists.newArrayList();
    private final CharSequence separator;


    private CommandLineArgument(List<String> arguments,
                                CharSequence separator) {
        this.arguments.addAll(arguments);
        this.separator = separator;
    }

    private CommandLineArgument(List<String> arguments) {
        this.arguments.addAll(arguments);
        this.separator = " ";
    }

    public static CommandLineArgument createFor(CharSequence separator,
                                                String... arguments) {
        return new CommandLineArgument(Lists.newArrayList(arguments), separator);
    }

    public static CommandLineArgument createFor(String... arguments) {
        return new CommandLineArgument(Lists.newArrayList(arguments));
    }

    public String asString() {
        return arguments.stream().collect(Collectors.joining(separator));
    }

    @Override
    public String toString() {
        return asString();
    }
}
