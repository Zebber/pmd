package be.vdcnathan.test.pmd;

import be.vdcnathan.test.pmd.git.GitClient;
import be.vdcnathan.test.pmd.output.PMDFileSampler;
import be.vdcnathan.test.pmd.output.SuperAwesomeMethodExtractorService;
import be.vdcnathan.test.pmd.pmd.PMDClient;
import be.vdcnathan.test.pmd.pmd.PMDFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;

/**
 * Created by nathanv on 26/05/2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Properties properties = getProperties();

        String pmdUrl = properties.getProperty("pmd.url");

        PMDClient pmdClient = new PMDFactory().getPMDClient(new URL(pmdUrl), properties.getProperty("pmd.rulesets"));

        Manager manager = new Manager(new GitClient(), pmdClient, new PMDFileSampler(new SuperAwesomeMethodExtractorService()));
        manager.readRepositories(new File("locations.txt"));
    }

    private static Properties getProperties() throws IOException {
        Properties properties = new Properties();

        properties.load(URLClassLoader.getSystemResourceAsStream("application.properties"));
        return properties;
    }
}
