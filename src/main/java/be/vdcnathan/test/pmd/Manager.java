package be.vdcnathan.test.pmd;

import be.vdcnathan.test.pmd.git.GitClient;
import be.vdcnathan.test.pmd.output.PMDFileSampler;
import be.vdcnathan.test.pmd.output.Sample;
import be.vdcnathan.test.pmd.pmd.PMDClient;
import be.vdcnathan.test.pmd.pmd.PMDFile;
import be.vdcnathan.test.pmd.pmd.PMDLine;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Manager {

    private final GitClient gitClient;
    private final PMDClient pmdClient;
    private PMDFileSampler pmdFileSampler;
    private int counter = 0;

    public Manager(GitClient gitClient,
                   PMDClient pmdClient, PMDFileSampler pmdFileSampler) {
        this.gitClient = gitClient;
        this.pmdClient = pmdClient;
        this.pmdFileSampler = pmdFileSampler;
    }


    public void readRepositories(File locationsFile) {
        if (!locationsFile.exists()) {
            throw new IllegalArgumentException("No locationsfile set");
        }

        try {
            List<String> locations = IOUtils.readLines(new FileReader(locationsFile));

            Map<String, List<PMDLine>> pmdFiles = locations
                    .stream()
                    .map(this::getFile)
                    .map(pmdClient::run)
                    .flatMap(List::stream)
                    .collect(Collectors.groupingBy(PMDLine::getFile));


            pmdFiles
                    .entrySet()
                    .stream()
                    .map(PMDFile::of)
                    .map(pmdFileSampler::sample)
                    .flatMap(Set::stream)
                    .forEach(this::saveSample);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveSample(Sample sample) {
        try {
            PrintWriter printWriter = new PrintWriter("samples/" + sample.getLabel() + "/" + sample.getLabel()+ "_sample_" + ++counter);
            printWriter.write(sample.getContent());
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getFile(String location) {
        if (isGitRepository(location)) {
            return gitClient.cloneRepository(location);
        } else {
            return new File(location);
        }
    }

    private boolean isGitRepository(String location) {
        return location.endsWith(".git");
    }
}
