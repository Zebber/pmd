package be.vdcnathan.test.pmd.git;

import be.vdcnathan.test.pmd.commandline.CommandLineArgument;
import be.vdcnathan.test.pmd.commandline.CommandLineExecutor;
import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * Created by nathanv on 29/05/2017.
 */
public class GitClient {

    public File cloneRepository(String location) {
        File cloningDirectory = FileUtils.getTempDirectory();
        new CommandLineExecutor("git")
                .runAt(cloningDirectory)
                .run(CommandLineArgument.createFor("clone", location));

        String repositoryName = location.substring(location.lastIndexOf('/'), location.length() - 4);

        return new File(cloningDirectory, repositoryName);
    }
}
