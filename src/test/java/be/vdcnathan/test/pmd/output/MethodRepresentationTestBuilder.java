package be.vdcnathan.test.pmd.output;

public class MethodRepresentationTestBuilder {

    private String content;
    private Integer startLine;
    private Integer endLine;
    private Integer width;
    private Integer height;

    public static MethodRepresentationTestBuilder aMethodRepresentation() {
        return new MethodRepresentationTestBuilder()
                .withContent("Default content")
                .withEndLine(-1)
                .withStartLine(-1);
    }

    public MethodRepresentation build() {
        return new MethodRepresentation(content, startLine, endLine, width, height);
    }

    public MethodRepresentationTestBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public MethodRepresentationTestBuilder withStartLine(Integer startLine) {
        this.startLine = startLine;
        return this;
    }

    public MethodRepresentationTestBuilder withEndLine(Integer endLine) {
        this.endLine = endLine;
        return this;
    }

    public MethodRepresentationTestBuilder withWidth(Integer width) {
        this.width = width;
        return this;
    }

    public MethodRepresentationTestBuilder withHeight(Integer height) {
        this.height = height;
        return this;
    }
}
