package be.vdcnathan.test.pmd.output;

import be.vdcnathan.test.pmd.pmd.PMDFile;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.Scanner;
import java.util.Set;

import static be.vdcnathan.test.pmd.output.SampleTestBuilder.aSample;
import static org.junit.Assert.*;

public class PMDFileSamplerTest {

    private PMDFileSampler pmdFileSampler = new PMDFileSampler(new SuperAwesomeMethodExtractorService());

    @Test
    public void sample() throws Exception {
        PMDFile pmdFile = new PMDFile("src/test/resources/exampleClass.java", Lists.newArrayList(32));

        Assertions.assertThat(
                pmdFileSampler.sample(pmdFile)
        ).contains(
                aSample()
                        .withContent("{\n" +
                                "    System.out.println(\"Hello world\");\n" +
                                "}")
                        .withLabel(Label.DIRTY)
                        .build(),
                aSample()
                        .withContent("{\n" +
                                "    String a = \"Werken aub.\";\n" +
                                "    return a;\n" +
                                "}")
                        .withLabel(Label.CLEAN)
                        .build()
        );
    }
}