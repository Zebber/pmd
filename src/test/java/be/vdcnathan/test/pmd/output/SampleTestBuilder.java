package be.vdcnathan.test.pmd.output;

public class SampleTestBuilder {
    private String content;
    private Label label;

    public static SampleTestBuilder aSample() {
        return new SampleTestBuilder();
    }

    public Sample build() {
        return new Sample(content, label);
    }

    public SampleTestBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public SampleTestBuilder withLabel(Label label) {
        this.label = label;
        return this;
    }
}
