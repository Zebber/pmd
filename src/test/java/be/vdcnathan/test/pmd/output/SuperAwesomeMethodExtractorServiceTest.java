package be.vdcnathan.test.pmd.output;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class SuperAwesomeMethodExtractorServiceTest {

    MethodExtractorService methodExtractorService = new SuperAwesomeMethodExtractorService();
    private String contentMethod1;
    private String contentMethod2;
    private String contentMethod3;
    private String contentMethod4;

    @Before
    public void setup() {
        contentMethod1 =
                "    public int methodOne() {\n" +
                "        int a = 1;\n" +
                "        int b = 2;\n" +
                "        int c = 4;\n" +
                "        int d = 8;\n" +
                "        int e = 16;\n" +
                "        int f = 32;\n" +
                "\n" +
                "        return a + b + c + d + e + f;\n" +
                "    }";

        contentMethod2 =
                "    private String methodTwo() {\n" +
                "        String a = \"h\";\n" +
                "        String b = \"a\";\n" +
                "            String c = \"l\";\n" +
                "        String d = \"l\";\n" +
                "        String e = \"o\";\n" +
                "        String f = \"!\";\n" +
                "\n" +
                "        return a + b + c + d + e + f;\n" +
                "    }";

        contentMethod3 =
                "    public static String methodThree() {\n" +
                "        String a = \"Werken aub.\";\n" +
                "\n" +
                "        return a;\n" +
                "    }";

        contentMethod4 =
                "    public void printHelloWorld() {\n" +
                "        System.out.println(\"Hello world\");\n" +
                "    }";
    }

    @Test
    public void extract() throws Exception {
        File file = new File("src/test/resources/exampleClass.java");
        System.out.println(file.getAbsolutePath());

        Set<MethodRepresentation> methodRepresentations = methodExtractorService.extract(file);

        assertThat(methodRepresentations).containsExactlyInAnyOrder(
                new MethodRepresentation(contentMethod1, 3, 12, 37, 10),
                new MethodRepresentation(contentMethod2, 14, 23, 37, 10),
                new MethodRepresentation(contentMethod3, 25, 29, 40, 5),
                new MethodRepresentation(contentMethod4, 31, 33, 42, 3)
        );
    }

}