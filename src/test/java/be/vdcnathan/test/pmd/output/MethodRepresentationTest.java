package be.vdcnathan.test.pmd.output;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class MethodRepresentationTest {

    @Test
    public void smallEnough() throws Exception {
        Assertions.assertThat(
                MethodRepresentationTestBuilder.aMethodRepresentation()
                        .withWidth(120)
                        .withHeight(40)
                        .build()
                .isSmallEnough()
        ).isTrue();
    }

    @Test
    public void toWide() throws Exception {
        Assertions.assertThat(
                MethodRepresentationTestBuilder.aMethodRepresentation()
                        .withWidth(121)
                        .withHeight(10)
                        .build()
                .isSmallEnough()
        ).isFalse();
    }

    @Test
    public void toHigh() throws Exception {
        Assertions.assertThat(
                MethodRepresentationTestBuilder.aMethodRepresentation()
                        .withWidth(10)
                        .withHeight(41)
                        .build()
                .isSmallEnough()
        ).isFalse();
    }
}