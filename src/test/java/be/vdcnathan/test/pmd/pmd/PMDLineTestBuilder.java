package be.vdcnathan.test.pmd.pmd;

public class PMDLineTestBuilder {
    private String file;
    private int line;
    private String cause;

    public static PMDLineTestBuilder aPMDLine() {
        return new PMDLineTestBuilder();
    }

    public PMDLine build() {
        return PMDLine.of(file, line, cause);
    }

    public PMDLineTestBuilder withFile(String file) {
        this.file = file;
        return this;
    }

    public PMDLineTestBuilder withLine(int line) {
        this.line = line;
        return this;
    }

    public PMDLineTestBuilder withCause(String cause) {
        this.cause = cause;
        return this;
    }
}
