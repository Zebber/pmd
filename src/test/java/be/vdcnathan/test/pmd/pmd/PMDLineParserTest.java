package be.vdcnathan.test.pmd.pmd;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Optional;

import static be.vdcnathan.test.pmd.pmd.PMDLineTestBuilder.aPMDLine;

public class PMDLineParserTest {

    private PMDLineParser pmdLineParser = new PMDLineParser();

    @Test
    public void parsePMDLine() throws Exception {
        Assertions.assertThat(
                pmdLineParser.parseLine(
                        "/tmp/butterknife/butterknife-compiler/src/main/java/butterknife/compiler/BindingSet.java:136:\tUseless parentheses."
                )
        ).isEqualTo(
                Optional.of(
                        aPMDLine()
                                .withFile("/tmp/butterknife/butterknife-compiler/src/main/java/butterknife/compiler/BindingSet.java")
                                .withLine(136)
                                .withCause("\tUseless parentheses.")
                                .build()
                )
        );
    }

    @Test
    public void parseEmptyLine() throws Exception {
        Assertions.assertThat(
            pmdLineParser.parseLine("")
        ).isEqualTo(
                Optional.empty()
        );
    }
}