public class exampleClass {

    public int methodOne() {
        int a = 1;
        int b = 2;
        int c = 4;
        int d = 8;
        int e = 16;
        int f = 32;

        return a + b + c + d + e + f;
    }

    private String methodTwo() {
        String a = "h";
        String b = "a";
            String c = "l";
        String d = "l";
        String e = "o";
        String f = "!";

        return a + b + c + d + e + f;
    }

    public static String methodThree() {
        String a = "Werken aub.";

        return a;
    }

    public void printHelloWorld() {
        System.out.println("Hello world");
    }

}
